<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Initial login route to the auth controller
Route::post('/login', 'AuthController@login');

// Route Group protected by sanctum authentication
Route::middleware(['auth:sanctum'])->group(function () {  
    // Gets the user if they are authenticated
    Route::get('/user', 'UserController@index');

    // Gets the list. Again only shows if authenticated with sanctum
    Route::get('/list', function () {
        return ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'];
    });

    // logs the current user out
    Route::post('/logout', 'AuthController@logout');
});



