
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/List.vue') },
      { path: '/login', name: 'login', component: () => import('pages/Login') },
      { path: '/list', name: 'list', component: () => import('pages/List') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
