import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import router from '../router/index'

// My Axios instance
const axios = Axios.create({
  baseURL: 'http://localhost:8000',
  withCredentials: true
})
Vue.use(Vuex)

const Store = new Vuex.Store({

  state: {
    list: null,
    authenticated: false,
    loginError: false
  },

  actions: {
    /*
      Login - Used to log the user into the SPA

      First will get the CSRF-Cookie from the server then
      sends the login form data to get our token.

      Once the token is returned from the response axios
      attaches it to it's headers for future requests.

      If an error is thrown the login page is updated
      to inform the user.
    */
    async login (context, details) {
      await axios.get('/sanctum/csrf-cookie')
      axios.post('/api/login', {
        email: details.email,
        password: details.password
      }).then((response) => {
        if (response.status === 200) {
          // eslint-disable-next-line
          axios.defaults.headers.common['Authorization'] = `Bearer ${response.data.access_token}`
          context.commit('setAuthenticated', true)
          context.commit('showLoginError', false)
          router.push('list')
        }
      }).catch((err) => {
        console.log(err)
        context.commit('showLoginError', true)
      })
    },

    /*
    Logout - function used to log the user out

    If accepted by the server it will remove the authorisation token
    from the Axios instance and clear any user data in the store.

    */
    logout (context) {
      axios.post('/api/logout').then(
        (response) => {
          delete axios.defaults.headers.common.Authorization
          context.commit('setAuthenticated', false)
          context.commit('setList', null)
          router.push('/login')
        }
      )
    },

    /*
    Simple test method used to show the user object can be retrieved
    from the protected API route.

    Will print to the web browser console with the user object data
    */
    testUserAuth (context) {
      axios.get('/api/user').then(
        (response) => {
          console.log(response.data)
        }
      )
    },

    /*
    fetchList - Fetches the list from the list API route.

    /api/list is a route with sanctum authentication enabled.
    The list will not show on the list page if the authentication
    token has been sent to the client.

    */
    fetchList (context) {
      axios.get('/api/list').then(
        (response) => {
          context.commit('setList', response.data)
        }
      )
    }
  },

  mutations: {

    setToken (state, value) {
      state.token = value
    },

    setList (state, value) {
      state.list = value
    },

    setAuthenticated (state, value) {
      state.authenticated = value
    },

    showLoginError (state, value) {
      state.loginError = value
    }
  },

  getters: {

    user: state => {
      return state.user
    },

    list: state => {
      return state.list
    },

    authenticated: state => {
      return state.authenticated
    },

    loginError: state => {
      return state.loginError
    }
  },

  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV
})

export default Store
