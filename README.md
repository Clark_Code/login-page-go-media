<h1> Go Media login SPA test </h1>

<p> Here is the repo for my technical test application for Go-Media. This read me contains the instructions to run the SPA your self. </p>

<h2> setup instructions </h2>

<h3> clone the repo </h3>
<p> When the repo is cloned you should have two seperate projects. The root directory contains all the Laravel specific code while the frontend folder is used to contain the Vue/Quasar code for the client side. </p>

<h3> Commands to run </h3>
<p> run `composer install` inside the root directory </p>
<p> run `npm install` inside the root directory</p>
<p> run `npm install` inside the "frontend" directory </p>
<p> run `quasar build` inside the "frontend" directory </p>

<h3> migrations and seeders </h3>
<p> To help setup the project for testing I have created some migrations and seeders. These initialise some of the auth components for Laravel/Sanctum as well as some test users.
<p> run `php artisan migrate` </p>
<p> run `php artisan db:seed --classname=UsersTableSeeder` create the test data </p>

<h3> building the project </h3>
<p> Finially run the following commands to build the project </p>
<p> run `npm build` inside the main directory</p>
<p> run `quasar dev` inside the "frontend" directory </p>
<p> run `php artisan serve` inside the main directory to serve the application </p>

<h3> Possible issues </h3>
<p> My SPA uses Sqlite3 for the database. Because Sqlite3 uses a file for the database instead of a server you may need to change the path to the database inside the .env file. Under DB_DATABASE attribute change it to the full path of the database file inside the database folder</p>