<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Support\Facades\Hash;

// Auth Controller used to authenticate users
class AuthController extends Controller
{
    // Function to take in login request
    // Uses Auth::attempt to check the hash
    // If the auth check returns true, create a token and log them in
    public function login(Request $request) {

        try{
            $details = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        

        $user = User::where('email', $request->email)->first();
        // If we fail our login attempt then return a 401
        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return response()->json([
                'status_code' => 401,
                'message' => 'unauthorised'
            ], 401);
        }
        // The check passed so we can fetch the user
        $user = User::where('email', $request->email)->first();

        // Create token and return the 200 response
        $tokenResult = $user->createToken('auth-token')->plainTextToken;      
        return response()->json([
            'status_code' => 200,
            'access_token' => $tokenResult,
            'token_type' => 'Bearer'
        ]);
        
        // If any exceptions are caught return a 500 error
        } catch(Exception $error){
            return response()->json([
                'status_code' => '500',
                'message' => $error->getMessage(),
                'error' => $error
            ], 500);
        }
    
    }

    public function logout (Request $request){
        Auth::user()->tokens()->where('id', $request->user()->id)->delete();
    }
}
