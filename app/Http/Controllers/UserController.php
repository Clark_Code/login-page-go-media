<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// User controller class to return the user
class UserController extends Controller
{
    // Returns the current user
    public function index (Request $request){
        return $request->user();
    }
}
