<?php

namespace App\Http\Middleware;

use App\ActivityLog;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LogActivity
{
    // Middleware applied to all routes when run.
    // Allows the user ID, url the user accessed, their IP address and time stamps
    // to be stored in the database
    public function handle(Request $request, Closure $next)
    {
        // Leave as 0 if the user isn't logged in (Guest)
        $user_id = 0;
        
        $user = Auth::guard('sanctum')->user();
        
        if($user != null){
            $user_id = $user->id;
        }
        
        $url = $request->url();
        $address = $request->ip();

        ActivityLog::create(['user_id' => $user_id, 'url' => $url, 'address' => $address, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);

        return $next($request);
    }
}
