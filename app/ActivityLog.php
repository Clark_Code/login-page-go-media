<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Eloquent model to model my activity log database resources
class ActivityLog extends Model
{
    protected $fillable = [
        'user_id',
        'url',
        'address',
        'created_at',
        'updated_at'
    ];
}
